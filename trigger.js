
var jenkinsapi = require('jenkins-api');
const { URL } = require('url');
var userName = "qa.automation";
var password = '%211password';
var baseUrl = 'http://' + userName + '@nonprod-jenkinsqa.gdn-app.com/job/PROJECT%20TEAM/job/E2E/job/PreProd%20Environment/';


var methods = {
    buildTrigger: function (squadName, jobName) {
        console.log("inside buildTrigger function " + squadName + " / " + jobName);
        var result = "null";
        if (squadName == jobName) {
            console.log("equal....")
            squadName = '';
        }
        else{
            baseUrl=baseUrl+"job/";
        }
        squadName=squadName.replaceAll(" ","%20");
        var url = baseUrl + squadName;
        console.log("url trigger : " + url);
        const jenkinsUrl = new URL(url);
        jenkinsUrl.password = password;
        var jenkins = jenkinsapi.init(jenkinsUrl.href);
        jenkins.build(jobName, { token: '113034428564879756f0a741903dc965b2' }, function (err, data) {
            if (err) {
                console.log("Error while triggering  " + jobName + "  " + err);
                result = err;
            } else {
                console.log(data)
                result = "success";
            }
        });
        while (result == "null") {
            require('deasync').runLoopOnce();
        }
        return result;
    },
    buildTriggerWithParam: function (squadName, jobName) {
        console.log("inside buildTriggerWithParam function " + squadName + " / " + jobName);
        var result = "null";
        if (squadName == jobName) {
            console.log("equal....")
            squadName = '';
        }
        else{
            baseUrl=baseUrl+"job/";
        }
        squadName=squadName.replaceAll(" ","%20");
        var url = baseUrl + squadName;
        console.log("url trigger : " + url);
        const jenkinsUrl = new URL(url);
        jenkinsUrl.password = password;
        var jenkins = jenkinsapi.init(jenkinsUrl.href);
        jenkins.build_with_params(jobName, { token: '113034428564879756f0a741903dc965b2' }, function (err, data) {
            if (err) {
                console.log("Error while triggering  " + jobName + "  " + err);
                result = err;
            } else {
                console.log(data)
                result = "success";
            }
        });
        while (result == "null") {
            require('deasync').runLoopOnce();
        }
        return result;
    },

};
exports.data = methods;


