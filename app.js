var MongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://localhost:27017/";
var express = require('express');
var app = express();
var fs = require('fs');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

var triggerFunction = require("./trigger.js")
var updateDb = require("./myDb.js")

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

// suad
app.get('/preProdSanityData', function (req, res) {
  updateDb.data.squadList();
  MongoClient.connect(mongoUrl, function (err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");
    dbo.collection("preProdSanity").find({}).toArray(function (err, result) {
      if (err) throw err;
      console.log("preProdSanity");
      updateDb.data.squadWithJobList(result);
      res.send(result);
    });
    db.close();
  });

});

app.get('/preProdSanity', function (req, res) {
  res.sendFile(__dirname + '/squad.html');
});

MongoClient.connect(mongoUrl, function (err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  dbo.collection("preProdSanityJobs").find({}).toArray(function (err, result) {
    if (err) throw err;
    result.forEach(function (item, i) {
      var squadNameOriginal = (item.name);
      var squadName = squadNameOriginal.replaceAll(" ", "-")
      updateDb.data.createTable(squadNameOriginal, item.jobs);

      app.get('/preProdSanity/' + squadName + '/data', function (req, res) {
        res.send(item.jobs);
      });
      app.get('/preProdSanity/' + squadName, function (req, res) {
        if ((item.jobs).length == 0) {
          res.sendFile(__dirname + '/jobHome.html');
        } else {
          res.sendFile(__dirname + '/jobs.html');
        }
      });
      jobs = item.jobs;
      url = item.url;
      jobs.forEach(function (job, i) {
        var jobName = job.replaceAll(" ", "-");
        app.get('/preProdSanity/' + squadName + '/' + jobName, function (req, res) {
          res.sendFile(__dirname + '/jobHome.html');
        });

      });

    });
  });
});

MongoClient.connect(mongoUrl, function (err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  dbo.collection("preProdSanityJobsBuildDetail").find({}).toArray(function (err, result) {
    if (err) throw err;
    result.forEach(function (item, i) {
      var squadNameOrginal = item.squadName;
      var jobNameOrginal = item.jobName;
      var squadName = (item.squadName).replaceAll(" ", "-");
      var jobName = (item.jobName).replaceAll(" ", "-");
      var baseLink = '/preProdSanity/' + squadName + '/' + jobName;
      if (squadName == jobName) {
        baseLink = '/preProdSanity/' + squadName;
      }
      
      app.get(baseLink + '/buildResultData', function (req, res) {
        updateDb.data.buildResult(squadNameOrginal, jobNameOrginal);
        MongoClient.connect(mongoUrl, function (err, db) {
          if (err) throw err;
          var dbo = db.db("mydb");
          dbo.collection("preProdSanityJobsBuildDetail").find({ "jobName": jobNameOrginal }).toArray(function (err, result) {
            res.send(result);
          });

        });
      });

      app.get(baseLink + '/buildResult', function (req, res) {
        res.sendFile(__dirname + '/buildResult.html');
      });

      app.get(baseLink + '/run', function (req, res) {
        var result = triggerFunction.data.buildTriggerWithParam(squadNameOrginal, jobNameOrginal);
        if (result == "success") {
          res.sendFile(__dirname + '/runStatus.html');
        }
        else if (result.includes("500")) {
          result = triggerFunction.data.buildTrigger(squadNameOrginal, jobNameOrginal);
          if (result == "success") {
            res.send("Running ...");
          }
          else {
            res.sendFile(__dirname + '/triggerIssue.html');
          }
        }
        else {
          res.sendFile(__dirname + '/triggerIssue.html');
        }
      });
      app.get(baseLink + '/runing', function (req, res) {
        res.sendFile(__dirname + '/runStatus.html');
      });

    });
    db.close();
  });

});

app.listen(8081);



