var MongoClient = require('mongodb').MongoClient;
var mongoUrl = "mongodb://localhost:27017/";
var userName = "qa.automation";
var password = '%211password';

var jenkinsapi = require('jenkins-api');
const { URL } = require('url');
const jenkinsUrl = new URL('http://' + userName + '@nonprod-jenkinsqa.gdn-app.com/job/PROJECT%20TEAM/job/E2E/job/PreProd%20Environment/');
const baseUrl = 'http://' + userName + '@nonprod-jenkinsqa.gdn-app.com/job/PROJECT%20TEAM/job/E2E/job/PreProd%20Environment/';
jenkinsUrl.password = password;
var jenkins = jenkinsapi.init(jenkinsUrl.href);
var request = require('request');

var methods = {
    squadList: function () {
        jenkins.all_jobs({ token: 'jenkins-token' }, function (err, resultData) {
            if (err) { return console.log(err); }
            MongoClient.connect(mongoUrl, function (err, db) {
                if (err) throw err;
                var dbo = db.db("mydb");
                resultData.forEach(function (item, i) {
                    var myquery = { "name": item.name };
                    var newvalues = {
                        $set: {
                            "name": item.name,
                            "url": item.url,
                            "class": item._class
                        }
                    }
                    dbo.collection('preProdSanity').updateOne(
                        myquery,
                        newvalues,
                        { upsert: true },
                    )
                });
                db.close();

            });

        });
    },
    squadWithJobList: function (squadList) {
        squadList.forEach(function (squad, i) {
            var url = squad.url;
            var squadName = squad.name;
            var type = squad.class;
            const jenkinsUrl = new URL(url);
            var jenkins = jenkinsapi.init(jenkinsUrl.href);
            var jobs = [];
            if (type.includes("Folder")) {
                jenkins.all_jobs({ token: 'jenkins-token' }, function (err, resultData) {
                    if (err) { return console.log(err); }
                    resultData.forEach(function (jobsList, i) {
                        jobs.push(jobsList.name);
                    });
                    MongoClient.connect(mongoUrl, function (err, db1) {
                        if (err) { console.log(err) };
                        var dbo = db1.db("mydb");
                        var myquery = { "name": squadName };
                        var newvalues = {
                            $set: {
                                "name": squadName,
                                "jobs": jobs,
                                "url": url
                            }
                        }
                        newData = newvalues;
                        dbo.collection('preProdSanityJobs').updateOne(
                            myquery,
                            newvalues,
                            { upsert: true },
                        )
                        db1.close();
                    });
                });
            }
            else {
                MongoClient.connect(mongoUrl, function (err, db1) {
                    if (err) { console.log(err) };
                    var dbo = db1.db("mydb");
                    var myquery = { "name": squadName };
                    var newvalues = {
                        $set: {
                            "name": squadName,
                            "jobs": jobs,
                            "url": url
                        }
                    }
                    newData = newvalues;
                    dbo.collection('preProdSanityJobs').updateOne(
                        myquery,
                        newvalues,
                        { upsert: true },
                    )
                    db1.close();
                });

            }
        });

    },
    buildResult: function (squadName, jobName) {
        var jobNameForUrl = jobName.replaceAll(" ", "%20");
        var squadNameForUrl = squadName.replaceAll(" ", "%20");
        var url;
        if(squadName==jobName){
            url = baseUrl;
        }
        else{
           url = baseUrl + "/job/" + squadNameForUrl;
        }
        const jenkinsUrl = new URL(url);
        jenkinsUrl.password = password;
        var jenkins = jenkinsapi.init(jenkinsUrl.href);
        var reportName = getReportName(jenkins, jobName);
        console.log(squadName + " >> " + jobName + " >> " + reportName);
        var reportUrl = (url) + "/job/" + jobNameForUrl + "/" + reportName + "/summary.txt";
        if (reportName.includes(null)) {
            passed = null;
            failed = null;
        }
        else {
            var body = get_source_at(reportUrl.replace(userName + "@", ""));
            if (body && body[0] != '<') {
                var array = body.toString().split("\n");
                var date = array[0].split(" ")[3];
                var length = array[3].split(" ").length;
                var passed = array[3].split(" ")[length - 1];
                length = array[5].split(" ").length;
                var failed1 = array[5].split(" ")[length - 1];
                length = array[6].split(" ").length;
                var failed2 = array[6].split(" ")[length - 1];
                var failed = Number(failed1) + Number(failed2);
                console.log("Result  :  " + passed + "   " + failed);
            }
        }
        var newvalues = {
            $set: {
                "squadName": squadName,
                "jobName": jobName,
                "passed": passed,
                "failed": failed,
            }
        }
        var myquery = { "jobName": jobName };
        MongoClient.connect(mongoUrl, function (err, db1) {
            if (err) { console.log(err) };
            var dbo = db1.db("mydb");

            dbo.collection('preProdSanityJobsBuildDetail').updateOne(
                myquery,
                newvalues,
                { upsert: true },
            )
            db1.close();
        });

    },
    createTable: function (squadName, jobs) {
        if(jobs.length==0){
            jobName=squadName;
            var newvalues = {
                $set: {
                    "squadName": squadName,
                    "jobName": jobName,
                    "passed": "0",
                    "failed": "0"
                }
            }
            var myquery = { "jobName": jobName };
            MongoClient.connect(mongoUrl, function (err, db1) {
                if (err) { console.log(err) };
                var dbo = db1.db("mydb");

                dbo.collection('preProdSanityJobsBuildDetail').updateOne(
                    myquery,
                    newvalues,
                    { upsert: true },
                )
                db1.close();
            });

        }
        jobs.forEach(function (jobName, i) {
            var newvalues = {
                $set: {
                    "squadName": squadName,
                    "jobName": jobName,
                    "passed": "0",
                    "failed": "0"
                }
            }
            var myquery = { "jobName": jobName };
            MongoClient.connect(mongoUrl, function (err, db1) {
                if (err) { console.log(err) };
                var dbo = db1.db("mydb");

                dbo.collection('preProdSanityJobsBuildDetail').updateOne(
                    myquery,
                    newvalues,
                    { upsert: true },
                )
                db1.close();
            });
        });

    }

};
exports.data = methods;

function getReportName(jenkins, jobName) {
    console.log("inside getReportName() ...");
    var reportName;
    jenkins.get_config_xml(jobName, function (err, data) {
        if (err) {
            reportName = null;
            return console.log(err + " in config of  " + jobName);
        }
        else {
            data = (data.split("<reportName>"))[1];
            data = data.split("</reportName>")[0];
            reportName = data.replaceAll("_", "_5f");
            reportName = reportName.replaceAll(" ", "_20");
        }

    });

    while (reportName === undefined) {
        require('deasync').runLoopOnce();
    }
    return reportName;
}

function get_source_at(url) {
    console.log("report for " + url);
    var source;
    request(url, function (error, response1, body) {
        if (error) { console.log(error + " at " + url) }
        source = body;
    });
    while (source === undefined) {
        require('deasync').runLoopOnce();
    }
    return source;
}



